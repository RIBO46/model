from django.urls import path
from . import views

urlpatterns = [
    path('sign', views.sign, name='sign'),
    path('login', views.login_request, name='login'),
    path("logout", views.logout_request, name= "logout"),
]