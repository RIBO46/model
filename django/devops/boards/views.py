from django.contrib.auth.models import User
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.template.loader import render_to_string
from .models import Board, Topic, Post


# from prometheus_client import start_http_server, Summary

# start_http_server(8100)
# sum = Summary('my_failures', 'Description of counter')

def board(request):
    board = Board.objects.all()
    # sum.observe(1)
    return render(request, 'forum.html', {
        'board': board
    })


def topic(request, topic):
    z = int(topic) - 1
    topic = Topic.objects.all()
    board = Board.objects.all()
    return render(request, 'topic.html', {
        'board': board[z],
        'topic': topic,
    })


def new_topic(request, topic):
    foo = Board.objects.all()
    keybord = int(topic) - 1
    keybord = foo[keybord]
    topic = Topic.objects.all()
    if request.method == 'POST':
        theme = request.POST['theme']
        message = request.POST['message']
        user = User.objects.first()
        if len(theme) > 1 and len(message) > 1:
            createtopic = Topic.objects.create(
                subject=theme,
                board=keybord,
                starter=user
            )
            return render(request, 'new_topic.html', context={'error': 1})
        return render(request, 'new_topic.html', {
            'topic': topic,
            'error': 2,
        })
    return render(request, 'new_topic.html', {
        'topic': topic,
    })
