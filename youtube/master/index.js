const express = require('express')
const app = express()
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');
const mongo = require('mongodb');
const util = require('util');
const execFile = util.promisify(require('child_process').execFile);
const axios = require('axios');
const fs = require("fs");
var ObjectID = require('mongodb').ObjectID;

const MongoClient = mongo.MongoClient;

app.use(express.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(fileUpload({
    createParentPath: true
}));

const port = 3000
const url = 234234
// let slaveURL = 234234
// let slaveURL = '234234234
// let path = '/mnt/efs/fs1/';
// let path = 'cookies/';

let count1Slave = 0;
let params = false;

MongoClient.connect(url, async function (err, db) {
    if (err) throw err;
    let dbo = db.db("youtube");
    await parseParams();


    // await dbo.collection("cookie").deleteMany({}); //TODO test


    // await exec();

    async function startSlaves() {
        let sum = await dbo.collection("cookie").countDocuments({status: 0});
        let tasks = await dbo.collection("tasks").find({status: 0}).toArray()
        for (let i = 0; i < tasks.length; i++) {
            sum = sum + (tasks[i].count - tasks[i].count_processing)
        }
        console.log(sum);
        let needsCount = sum / params.limitTask - await dbo.collection("slaves").countDocuments();

        for (let i = 0; i < needsCount; i++) {
            let result = require('child_process').execSync('aws ec2 run-instances --image-id ami-123123123123213 --count 1 --instance-type t3.micro --key-name youtube --security-group-ids sg-123123123123 --subnet-id subnet-123123123123 --user-data /usr/bin/user-data').toString();
            // console.log(result);

            var i1 = result.indexOf('InstanceId: '), i2 = result.indexOf('InstanceType');
            let instanceId = result.substr(i1 + 12, i2 - (i1 + 12))

            var i1 = result.indexOf('PrivateIpAddress: '), i2 = result.indexOf('PrivateIpAddresses:');
            let ip = result.substr(i1 + 18, i2 - (i1 + 18))

            console.log('instanceId', instanceId) // i-

            if (instanceId.indexOf('i-') === 0) {
                result = require('child_process').execSync('aws elb register-instances-with-load-balancer --load-balancer-name youtube-lb --instances ' + instanceId).toString();
                console.log(result);
                await dbo.collection("slaves").insertOne({
                    instanceId: instanceId,
                    ip: ip
                });
            }
        }


    }

    async function stopSlave(data) {
        console.log('stopSlave', data)
    }


    taskManger();

    async function taskManger() {
        let taskLimit = params.taskLimitRest;
        let cookieLimit = params.cookieLimitRest;
        // let proxyLimit = 5;

        let issetObject = {
            cookie: 0,
            tasks: 0
        }

        let account_delay = 0;

        let sumSlave = await dbo.collection("slaves").countDocuments({});
        if(count1Slave < params.limitTask && sumSlave === 0) {

            try {
                let settings = await dbo.collection("settings").findOne({});
                // console.log(settings)
                if (settings !== null) {
                    account_delay = settings.account_delay;
                }

            } catch (e) {
                console.log(e)
            }

            for (let Y = 0; Y <= sumSlave; Y++) {
                //COOKIE CHECKER
                try {
                    let collectionCookie = await dbo.collection("cookie");
                    let cookies = await collectionCookie.find({status: 0}).limit(cookieLimit).toArray()
                    console.log(cookies);

                    let data = [];
                    for (let i = 0; i < cookies.length; i++) {
                        data.push({
                            id: cookies[i]._id,
                            filename: cookies[i].filename,
                            status: cookies[i].status,
                            type: 'cookie',
                        })
                    }

                    if (data.length > 0) {
                        let response = await axios.post(params.slaveURL, {data: data});
                        console.log(response.data)

                        for (let i = 0; i < cookies.length; i++) {
                            //TODO updateMany AND AFTER AXIOS !!!!
                            await collectionCookie.updateOne({_id: cookies[i]._id}, {$set: {status: 1}});
                        }

                        issetObject.cookie = data.length;
                    }
                } catch (e) {
                    console.log(e)
                }

                //TASK
                try {

                    let collectionTasks = await dbo.collection("tasks");
                    let tasks = await collectionTasks.find({status: 0}).limit(taskLimit).toArray()


                    let data = [];
                    for (let i = 0; i < tasks.length; i++) {
                        let delta = tasks[i].count - tasks[i].count_processing;
                        if (delta > 0) {
                            let collectionCookie = await dbo.collection("cookie");
                            let tte = Date.now() - account_delay * 1000;
                            let cookies = [];
                            if (tasks[i].type === 'like') {
                                cookies = await collectionCookie.find({
                                    status: 2,
                                    likeIds: {$ne: tasks[i].video_id},
                                    timestamp: {$lte: tte}
                                }).limit(delta).toArray()
                            } else if (tasks[i].type === 'subscribe') {
                                cookies = await collectionCookie.find({
                                    status: 2,
                                    subscribeIds: {$ne: tasks[i].video_id},
                                    timestamp: {$lte: tte}
                                }).limit(delta).toArray()
                            } else if (tasks[i].type === 'comment') {
                                cookies = await collectionCookie.find({
                                    status: 2,
                                    commentIds: {$ne: tasks[i].video_id},
                                    timestamp: {$lte: tte}
                                }).limit(delta).toArray()
                            }
                            for (let j = 0; j < cookies.length; j++) {
                                if (tasks[i].type === 'comment') {
                                    data.push({
                                        id_task: tasks[i]._id,
                                        type: tasks[i].type,
                                        video_id: tasks[i].video_id,
                                        comment: tasks[i].comment,
                                        filename: cookies[j].filename,
                                    })
                                } else {
                                    data.push({
                                        id_task: tasks[i]._id,
                                        type: tasks[i].type,
                                        video_id: tasks[i].video_id,
                                        filename: cookies[j].filename,
                                    })
                                }

                            }
                        }
                    }

                    if (data.length > 0) {
                        console.log('data', data)

                        let response = await axios.post(params.slaveURL, {data: data});
                        console.log(response.data)

                        let tasksArray = {};
                        for (let i = 0; i < data.length; i++) {
                            if (typeof tasksArray[data[i].id_task] !== "undefined") {
                                tasksArray[data[i].id_task] = tasksArray[data[i].id_task]++;
                            } else {
                                tasksArray[data[i].id_task] = 1;
                            }
                            //TODO updateMany AND AFTER AXIOS !!!!
                            let collectionCookie = await dbo.collection("cookie");
                            if (data[i].type === 'like') {
                                await collectionCookie.updateOne({filename: data[i].filename}, {
                                    $set: {timestamp: Date.now()},
                                    $push: {likeIds: data[i].video_id}
                                });
                            } else if (data[i].type === 'subscribe') {
                                await collectionCookie.updateOne({filename: data[i].filename}, {
                                    $set: {timestamp: Date.now()},
                                    $push: {subscribeIds: data[i].video_id}
                                });
                            } else if (data[i].type === 'comment') {
                                await collectionCookie.updateOne({filename: data[i].filename}, {
                                    $set: {timestamp: Date.now()},
                                    $push: {commentIds: data[i].video_id}
                                });
                            }

                        }


                        // console.log('tasksArray', tasksArray)
                        for (let key in tasksArray) {
                            // console.log(key);
                            let result = await dbo.collection("tasks").updateOne({_id: ObjectID(key)}, {$inc: {count_processing: tasksArray[key]}});
                        }

                        issetObject.tasks = data.length;
                    }


                } catch (e) {
                    console.log(e)
                }
            }
            if (issetObject.tasks !== 0 || issetObject.cookie !== 0) {
                if (sumSlave > 0) {
                    count1Slave = 0;
                } else {
                    count1Slave = count1Slave + (issetObject.tasks + issetObject.cookie);
                }


                await startSlaves();
            }
        }
        setTimeout(async function () {
            await taskManger();
        }, 20000);
    }


//START---------------------------------TASKS-----------------------------------
    /*
    Добавление задачи
    JSON
    {
        "type": "like", //like, subscribe, comment
        "video_id": "WLhaPQvCExs",
        "count": 10,
        "comment": "good video" //Если тип - комментарий
    }
    */
    app.post('/add_task', async (req, res) => {
        let response = false;
        try {

            //TODO
            let task = req.body;
            task['count_processing'] = 0;
            task['count_completed'] = 0;
            task['status'] = 0;
            console.log(task)
            let collection = dbo.collection("tasks");
            let result = await collection.insertOne(task);

            console.log(result);

            let typeString = task.type + 'Tasks'
            let collectionType = dbo.collection(typeString);


            // console.log(result)


            response = {
                code: 200,
                message: 'GOOD'
            }
        } catch (e) {
            response = {
                code: 500,
                message: e
            }
        }
        res.send(response)
    })

    /*
    Описание задач
    QUERY

    */
    app.get('/get_task', async (req, res) => {
        let response = false;
        try {
            // console.log(req.body)

            let data = await dbo.collection("tasks").find({}).toArray();
            console.log(data);

            response = {
                code: 200,
                data: data
            }
        } catch (e) {
            response = {
                code: 500,
                message: e
            }
        }
        res.send(response)
    })
//END---------------------------------TASKS-----------------------------------


//START---------------------------------ACCOUNTS-----------------------------------
    /*
    Добавление кук
    form-data
    {
        "provider": "test",
        "cookies": [],
    }
    */
    app.post('/add_cookies', async (req, res) => {
        let response = false;
        try {
            if (req.files) {
                // console.log(req.files.cookies)
                console.log(req.body)

                let provider = req.body.provider;
                let cookies = [];

                if (!Array.isArray(req.files.cookies)) {
                    let temp = req.files.cookies;
                    req.files.cookies = []
                    req.files.cookies.push(temp);
                }
                for (let i = 0; i < req.files.cookies.length; i++) {
                    let cookie = req.files.cookies[i];
                    // console.log(cookie)
                    let filename = Date.now() + '_' + (Math.floor(Math.random() * 1000) + 1000).toString() + '.txt';
                    await cookie.mv(params.path + filename);
                    cookies.push({
                        name: cookie.name,
                        filename: filename,
                        provider: provider,
                        timestamp: 0,
                        likeIds: [],
                        subscribeIds: [],
                        commentIds: [],
                        status: 0
                    })
                }

                let collection = dbo.collection("cookie");
                let result = await collection.insertMany(cookies);


                //TODO
                response = {
                    code: 200,
                    message: 'GOOD'
                }

            } else {
                response = {
                    code: 500,
                    message: 'No file uploaded'
                }
            }

        } catch (e) {
            console.log(e)
            response = {
                code: 500,
                message: e
            }
        }
        res.send(response)
    })

    /*
    Добавление аккаунта
    JSON
    {
        "provider": "test",
        "accounts": [
            {
                "login": "test",
                "psw": "test",
            },
            ...
        ],
    }
    */
    app.post('/add_accounts', (req, res) => {
        let response = false;
        try {
            console.log(req.body)

            //TODO

            response = {
                code: 200,
                data: 'GOOD'
            }
        } catch (e) {
            response = {
                code: 500,
                message: e
            }
        }
        res.send(response)
    })

    /*
    Получение невалидных аккаунтов
    QUERY

    */
    app.get('/get_valid_accounts', async (req, res) => {
        let response = false;
        try {
            // console.log(req.query)

            let data = [];
            if (req.query.valid === 'true') {
                data = await dbo.collection("cookie").find({status: 2}).toArray();
            } else {
                data = await dbo.collection("badCookie").find().toArray();
            }

            response = {
                code: 200,
                data: data
            }
        } catch (e) {
            response = {
                code: 500,
                message: e
            }
        }
        res.send(response)
    })

//END---------------------------------ACCOUNTS-----------------------------------


//START---------------------------------PROXY-----------------------------------
    /*
    Добавление аккаунта
    JSON
    {
        "provider": "test",
        "treads_count": 1,
        "timeout": 10,
        "proxy": [
            {
                "proxy": "127.0.0.1:5000",
                "type": "socks5",
            },
            ...
        ],
    }
    */
    app.post('/add_proxy', (req, res) => {
        let response = false;
        try {
            console.log(req.body)

            //TODO

            response = {
                code: 200,
                message: 'GOOD'
            }
        } catch (e) {
            response = {
                code: 500,
                message: e
            }
        }
        res.send(response)
    })

    /*
    Получение невалидных проксей
    QUERY

    */
    app.get('/get_valid_proxy', (req, res) => {
        let response = false;
        try {
            console.log(req.body)

            //TODO

            response = {
                code: 200,
                data: []
            }
        } catch (e) {
            response = {
                code: 500,
                message: e
            }
        }
        res.send(response)
    })


//END---------------------------------PROXY-----------------------------------


//START---------------------------------SETTINGS-----------------------------------
    /*
    Добавление аккаунта
    JSON
    {
        "account_delay": 10
    }
    */
    app.post('/settings', async (req, res) => {
        let response = false;
        try {
            console.log(req.body)

            let settings = await dbo.collection("settings").findOne({});
            if (settings === null) {
                await dbo.collection("settings").insertOne(req.body);
            } else {
                await dbo.collection("settings").updateOne({}, {$set: {account_delay: req.body.account_delay}});
            }

            response = {
                code: 200,
                message: 'GOOD'
            }
        } catch (e) {
            response = {
                code: 500,
                message: e
            }
        }
        res.send(response)
    })

    app.get('/get_slaves', async (req, res) => {
        let response = false;
        try {
            let slaves = await dbo.collection("slaves").find().toArray();
            slaves.push({
                name: '1 slave'
            })
            response = {
                code: 200,
                data: slaves
            }
        } catch (e) {
            response = {
                code: 500,
                message: e
            }
        }
        res.send(response)
    })

//END---------------------------------SETTINGS-----------------------------------


//START---------------------------------CALLBACK-----------------------------------

    app.post('/callback', async (req, res) => {
        let response = false;
        try {

            let data = req.body.data;
            if (data.type === 'stop') {
                await stopSlave(data)
            } else if (data.type === 'cookie') {
                if (count1Slave > 0) {
                    count1Slave--;
                }
                if (data.status === 3) {
                    let coo = await dbo.collection("cookie").findOne({filename: data.filename});
                    console.log(data.filename, '- bad')
                    if (coo !== null) {
                        await dbo.collection("badCookie").insertOne({name: coo.name, provider: coo.provider});
                        await dbo.collection("cookie").deleteOne({filename: data.filename});
                        try {
                            await fs.unlinkSync(params.path + coo.filename)
                        } catch (e) {

                        }
                    }
                } else {
                    console.log(data.filename, '- good')
                    await dbo.collection("cookie").updateOne({filename: data.filename}, {$set: {status: data.status}});
                }
            } else if (data.type === 'like' || data.type === 'subscribe' || data.type === 'comment') {
                let task = await dbo.collection("tasks").findOne({_id: ObjectID(data.id_task)});
                if (task != null) {
                    let status = 0;
                    if (task.count <= task.count_completed + 1) {
                        status = 2;
                    }
                    if (data.status === 2) {
                        if (count1Slave > 0) {
                            count1Slave--;
                        }
                        console.log(data.type, '(' + data.video_id + ') - good')
                        await dbo.collection("tasks").updateOne({_id: ObjectID(data.id_task)}, {
                            $set: {
                                status: status,
                                count_completed: task.count_completed + 1,
                            }
                        });
                    } else {
                        console.log(data.type, '(' + data.video_id + ') - bad')
                        await dbo.collection("tasks").updateOne({_id: ObjectID(data.id_task)}, {
                            $set: {
                                status: status,
                                count_processing: task.count_processing - 1,
                            }
                        });

                    }
                }
            }

            response = {
                code: 200,
                data: []
            }
        } catch (e) {
            console.log(e)
            response = {
                code: 500,
                message: e
            }
        }
        res.send(response)
    })

//END---------------------------------CALLBACK-----------------------------------

    app.listen(port, () => {
        console.log(`App listening on port ${port}`)
    })

});


async function parseParams() {
    let pathParams = 'config.json';
    if (fs.existsSync(pathParams)) {
        try {
            let data = fs.readFileSync(pathParams, 'UTF-8');
            params = JSON.parse(data);
        } catch (e) {
            return 'Файл "' + pathParams + '" не верен!';
        }
    } else {
        return 'Файл "' + pathParams + '" не найден!';
    }
}

