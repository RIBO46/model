const fs = require('fs');
const {firefox, chromium} = require('playwright');
const {parse_cookies} = require("./parse_cookies");
const axios = require('axios');
const express = require('express')
var HttpsProxyAgent = require('https-proxy-agent');
const SocksProxyAgent = require('socks-proxy-agent');
// myIP = require('my-ip');
// console.dir(myIP());


const app = express()
app.use(express.json());

const port = 3001
let taskArray = [];
let activeThreadsCount = 0;

let stop = false;
let params = false;
let browser = false;
let browserProxy = false;

init();
// let masterURL = 123123123
// let path = '../master/cookies/';
// let threadsCount = 4;
async function init() {
    await parseParams()

    browser = await chromium.launch({
        headless: true,
        defaultViewport: true,
        // executablePath: executablePath,
    });

    browserProxy = await chromium.launch({
        headless: true,
        defaultViewport: true,
        proxy: {server: 'per-context'}
        // executablePath: executablePath,
    });


    await run();
}


app.post('/add_task', async (req, res) => {
    let response = false;
    try {

        let request = req.body;
        taskArray = taskArray.concat(request.data)

        response = {
            code: 200,
            message: 'GOOD'
        }
    } catch (e) {
        response = {
            code: 500,
            message: e
        }
    }
    await res.send(response)
});

async function run() {
    // console.log('run()');
    // console.log(taskArray.length);
    for (let i = activeThreadsCount; i < params.threadsCount; i++) {
        setTimeout(async function () {
            if (taskArray.length <= 0) {
                //TODO axios end work
                // activeThreadsCount--;
            } else {
                activeThreadsCount++;
                if (taskArray[0].type === 'cookie') {
                    let data = taskArray[0];
                    taskArray.splice(0, 1);
                    let result = await checkCookie(data);
                } else if (taskArray[0].type === 'proxy') {
                    let data = taskArray[0];
                    taskArray.splice(0, 1);
                    let result = await checkProxy(data);
                } else {
                    let data = taskArray[0];
                    taskArray.splice(0, 1);
                    let result = await task(data);
                }
                activeThreadsCount--;
            }
        }, i * 10);
    }

    if (taskArray.length === 0) {
        if (!stop) {
            stop = true;
            stopSlave()
        }
    } else {
        stop = false;
    }


    setTimeout(async function () {
        await run(); //TODO Запускать не в цикле
    }, 5000);

}

function stopSlave() {
    try {
        setTimeout(async function () {
            if (taskArray.length === 0) {
                let data = {
                    type: 'stop',
                    // ip: myIP(),
                }
                await axios.post(params.masterURL, {data: data});
            }
        }, params.deadTime * 1000);
    } catch (e) {
        console.log(e)
    }
}

async function checkProxy(data) {
    console.log(data.type, data.proxy)
    try {
        let httpsAgentGlobal;
        if (data.proxy_type == 'http' || data.proxy_type == 'https') {
            let arrayOfStrings = data.proxy.split(':');
            httpsAgentGlobal = new HttpsProxyAgent({
                host: arrayOfStrings[0],
                port: arrayOfStrings[1],
                auth: `${data.login}:${data.password}`
            })
        } else {
            httpsAgentGlobal = new SocksProxyAgent(`${data.proxy_type}://${data.login}:${data.password}@${data.proxy}/`)
        }

        try {
            await axios.get('https://google.com', {
                httpsAgent: httpsAgentGlobal
            });
            data.status = 2;
        } catch (e) {
            data.status = 3;
        }

    } catch (e) {
        data.status = 4;
        data.error = e;
        console.log(e)
    }
    await axios.post(params.masterURL, {data: data});
    return true;
}

async function checkCookie(data) {
    console.log(data.type, data.filename)
    // let browser = false;
    let context = false;
    try {
        // browser = await firefox.launch({
        //     headless: true,
        //     defaultViewport: true,
        //     // executablePath: executablePath,
        // });

        context = await browser.newContext();
        let json_cookies = parse_cookies(params.path + data.filename)
        for (const item of json_cookies) {
            try {
                item['secure'] = true;
                await context.addCookies([item]);
            } catch (err) {
                // console.log(err);
            }
        }
        let page = await context.newPage();
        await page.goto(`https://google.com/`);

        await page.waitForTimeout(500)


        try {
            await page.locator('//*[@id="gb"]/div/div[2]/div[2]/div/a/img').waitFor({timeout: 500});
            data.status = 2;
        } catch (e) {
            data.status = 3;
        }
        await axios.post(params.masterURL, {data: data});

        try {
            await context.close();
            // await browser.close();
        } catch (e) {
            console.log(e)
        }

    } catch (e) {
        data.status = 4;
        data.error = e;
        try {
            await axios.post(params.masterURL, {data: data});
        } catch (e) {

        }
        console.log(e)
        try {
            await context.close();
            // await browser.close();
        } catch (e) {
            console.log(e)
        }
    }
    return true;
}

async function task(data) {
    console.log(data.type, data.video_id)
    // console.log(data)
    let browserLocal = false;
    let context = false;

    try {
        if (data.proxy) {
            let server = data.proxy.type + '://' + data.proxy.proxy;
            if (data.proxy.login) {
                let server = data.proxy.type + '://' + data.proxy.proxy;
                context = await browserProxy.newContext({
                    proxy: {
                        server: server,
                        username: data.proxy.login,
                        password: data.proxy.password
                    }
                });
            } else {
                context = await browserProxy.newContext({
                    proxy: {
                        server: server,
                    }
                });
            }
        } else {
            context = await browser.newContext({});
        }


        let json_cookies = parse_cookies(params.path + data.filename)
        for (const item of json_cookies) {
            try {
                item['secure'] = true;
                await context.addCookies([item]);
            } catch (err) {
                console.log('coo', err);
            }
        }
        let page = await context.newPage();
        await page.goto(`https://www.youtube.com/watch?v=${data.video_id}/`, {timeout: 60000});
        await page.waitForTimeout(2000)


        if (data.type === 'like') {
            /**
             * Like
             */
            await page.click('//*[@id="top-level-buttons-computed"]/ytd-toggle-button-renderer[1]')
        } else if (data.type === 'subscribe') {
            /**
             * Subscribe
             */
            await page.click('//*[@id="subscribe-button"]/ytd-subscribe-button-renderer')
        } else if (data.type === 'comment') {
            /**
             * Comment
             */
            await page.mouse.wheel(0, 1500)
            await page.click('#simplebox-placeholder')
            await page.waitForTimeout(1000)
            await page.type('#contenteditable-root', data.comment)
            await page.waitForTimeout(1000)
            await page.click('//*[@id="submit-button"]')
        }
        await page.waitForTimeout(2000)
        try {
            await context.close();
            // await browserLocal.close();
        } catch (e) {
            console.log(e)
        }


        data.status = 2;
        await axios.post(params.masterURL, {data: data});

    } catch (e) {
        data.status = 4;
        data.error = e;
        try {
            await axios.post(params.masterURL, {data: data});
        } catch (e) {
        }
        console.log(e)
        try {
            await context.close();
            // await browserLocal.close();
        } catch (e) {
            console.log(e)
        }
    }
    return true;
}


app.listen(port, () => {
    console.log(`App listening on port ${port}`)
})


async function parseParams() {
    let pathParams = 'config.json';
    if (fs.existsSync(pathParams)) {
        try {
            let data = fs.readFileSync(pathParams, 'UTF-8');
            params = JSON.parse(data);
        } catch (e) {
            return 'Файл "' + pathParams + '" не верен!';
        }
    } else {
        return 'Файл "' + pathParams + '" не найден!';
    }
}
