const mongo = require('mongodb');
const MongoClient = mongo.MongoClient;

const url = 'mongodb://localhost:27017';

// MongoClient.connect(url, { useNewUrlParser: true }, (err, client) => {
MongoClient.connect(url, async function (err, db) {

    if (err) throw err;
    let dbo = db.db("youtube");
    try {
        await dbo.createCollection("tasks");
    } catch (e) {
    }
    try {
        await dbo.createCollection("cookie");
    } catch (e) {
    }
    try {
        await dbo.createCollection("accounts");
    } catch (e) {
    }
    try {
        await dbo.createCollection("proxy");
    } catch (e) {
    }
    try {
        await dbo.createCollection("settings");
    } catch (e) {
    }
    try {
        await dbo.createCollection("servers");
    } catch (e) {
    }
    try {
        await dbo.createCollection("badCookie");
    } catch (e) {
    }
    try {
        await dbo.createCollection("slaves");
    } catch (e) {
    }

    await db.close();

    console.log('GOOD')

});