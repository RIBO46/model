from django.urls import path
from . import views

urlpatterns = [
    path('board/', views.board, name='board'),
    path('board/<topic>', views.topic, name='topic'),
    #path('board/<topic>/<subject>', views.subject, name='topic-subject'),
    path(r'^board/(<topic>)/new$', views.new_topic, name='new_topic'),
]